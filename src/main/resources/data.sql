INSERT INTO app_user (id, password, username, access)
VALUES (1, '{bcrypt}$2a$10$ojP8thD/VgmywDC8w0B7He5cCJx1hN444..Soqw1Hwt/9ZChuDZCC', 'shakvoort', 'kalshoven');

INSERT INTO customer(customerid, name, email) values (100, "racknet", "info@racknet.nl");
INSERT INTO customer(customerid, name, email) values (200, "kalshoven", "info@kalshoven.nl");
INSERT INTO customer(customerid, name, email) values (300, "hakvoort development", "sven.hakvoort@gmail.com");
INSERT INTO customer(customerid, name, email) values (400, "kimarka", "kimberley23@live.nl");

