package nl.racknet.kalshoven.klantenservice.control;

import nl.racknet.kalshoven.klantenservice.domain.Customer;
import nl.racknet.kalshoven.klantenservice.config.multitenant.TenantContext;
import nl.racknet.kalshoven.klantenservice.respository.CustomerRepository;
import nl.racknet.kalshoven.klantenservice.service.CustomerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/v1/customers")
public class CustomerController {

    @Autowired
    private CustomerService customerService;

    @PutMapping("")
    public Customer addNewCustomer(Customer customer) {
        return customerService.save(customer);
    }


    @PatchMapping("/{id}")
    public ResponseEntity updateCustomer(Customer customer) {
        return new ResponseEntity<>(customerService.update(customer), HttpStatus.CREATED);
    }


    @DeleteMapping("/{id}")
    public ResponseEntity deleteCustomer(@PathVariable Integer id) {
        Customer customer = customerService.findOneById(id);
        customerService.delete(customer);
        return ResponseEntity.ok("Customer deleted");
    }


    @GetMapping(path="/{id}")
    public Customer getCustomer(@PathVariable Integer id) {
        return customerService.findOneById(id);
    }


    @GetMapping(path="")
    public @ResponseBody Iterable<Customer> getAllCustomers() {
        return customerService.findAll();
    }


}
