package nl.racknet.kalshoven.klantenservice.control.intercept;

import nl.racknet.kalshoven.klantenservice.config.multitenant.TenantContext;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class CustomInterceptor extends HandlerInterceptorAdapter {

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object object) throws Exception {
        try {
            Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
            TenantContext.setCurrentTenant(authentication.getAuthorities().toArray()[0].toString());
        } catch (NullPointerException ignored) {

        }
        return true;
    }
}
