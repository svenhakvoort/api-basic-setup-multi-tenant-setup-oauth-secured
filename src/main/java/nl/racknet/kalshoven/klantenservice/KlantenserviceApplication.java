package nl.racknet.kalshoven.klantenservice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@SpringBootApplication
public class KlantenserviceApplication {

	public static void main(String[] args) {
		SpringApplication.run(KlantenserviceApplication.class, args);
	}

}
