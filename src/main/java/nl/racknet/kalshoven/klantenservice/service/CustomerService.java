package nl.racknet.kalshoven.klantenservice.service;

import nl.racknet.kalshoven.klantenservice.exception.CustomerNotFoundException;
import nl.racknet.kalshoven.klantenservice.domain.Customer;
import nl.racknet.kalshoven.klantenservice.respository.CustomerRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class CustomerService {

    @Autowired
    private CustomerRepository customerRepository;

    public Customer findOneById(int id) {
        Customer customer = customerRepository.findOneByCustomerID(id);
        if (null == customer) {
            throw new CustomerNotFoundException();
        }
        return customer;
    }

    public Customer findOneByName(String name) {
        Customer customer = customerRepository.findOneByName(name);
        if (null == customer) {
            throw new CustomerNotFoundException();
        }
        return customer;
    }

    public Customer save(Customer customer) {
        return update(customer);
    }

    public Customer update(Customer customer) {
        try {
            Customer existingCustomer = findOneByName(customer.getName());
            customer.setCustomerID(existingCustomer.getCustomerID());
        } catch (CustomerNotFoundException ignored) {

        }
       return customerRepository.save(customer);
    }

    public void delete(Customer customer) {
        customerRepository.delete(customer);
    }

    public void deleteById(int id) {
        customerRepository.delete(findOneById(id));
    }

    public Iterable<Customer> findAll() {
        return customerRepository.findAll();
    }

}
