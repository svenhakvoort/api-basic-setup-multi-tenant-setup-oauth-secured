package nl.racknet.kalshoven.klantenservice.domain;

public enum UserRole {
    ADMIN,
    USER
}
