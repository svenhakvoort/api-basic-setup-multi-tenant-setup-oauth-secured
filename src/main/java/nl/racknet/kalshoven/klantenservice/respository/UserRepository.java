package nl.racknet.kalshoven.klantenservice.respository;

import nl.racknet.kalshoven.klantenservice.domain.User;
import org.springframework.data.repository.CrudRepository;

public interface UserRepository extends CrudRepository<User, Long> {

    User findByUsername(String username);

}
