package nl.racknet.kalshoven.klantenservice.respository;


import nl.racknet.kalshoven.klantenservice.domain.Customer;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

public interface CustomerRepository extends CrudRepository<Customer, Long> {


    Customer findOneByCustomerID(@Param("id") Integer id);

    Customer findOneByName(@Param("name") String name);
}
