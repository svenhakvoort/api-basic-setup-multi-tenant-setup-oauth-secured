package nl.racknet.kalshoven.klantenservice.control;

import nl.racknet.kalshoven.klantenservice.KlantenserviceApplication;
import nl.racknet.kalshoven.klantenservice.domain.Customer;
import nl.racknet.kalshoven.klantenservice.service.CustomerService;
import org.hamcrest.Matchers;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import java.util.ArrayList;
import java.util.List;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = { KlantenserviceApplication.class })
@SpringBootTest
@ActiveProfiles("test")
public class CustomerControllerTest {

    @Autowired
    private WebApplicationContext wac;

    @MockBean
    private CustomerService customerService;

    private MockMvc mockMvc;

    @Before
    public void setup() throws Exception {
        this.mockMvc = MockMvcBuilders.webAppContextSetup(this.wac).build();
    }

    private final Customer firstCustomer = new Customer("Racknet", "sven.hakvoort@gmail.com");
    private final Customer secondCustomer = new Customer("Kalshoven", "info@kalshoven.nl");
    private final String exampleCustomerJson = "{\"name\":\"Racknet\", \"email\":\"sven.hakvoort@gmail.com\"}";

    @Test
    public void addNewCustomer() throws Exception {

        Mockito.when(customerService.save(Mockito.any(Customer.class))).thenReturn(firstCustomer);

        this.mockMvc.perform(put("/v1/customers")
                .accept(MediaType.APPLICATION_JSON)
                        .content(exampleCustomerJson).characterEncoding("UTF-8")
                .contentType(MediaType.APPLICATION_JSON))

                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath(".email").value(firstCustomer.getEmail()));
    }

    @Test
    public void getCustomer() throws Exception {

        Mockito.when(customerService.findOneById(Mockito.anyInt())).thenReturn(new Customer("kimarka", "kimberley23@live.nl"));

                this.mockMvc.perform(get("/v1/customers/400"))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath(".name").value("kimarka"))
                .andExpect(MockMvcResultMatchers.jsonPath(".email").value("kimberley23@live.nl"));
    }

    @Test
    public void getAllCustomers() throws Exception {
        List<Customer> customers = new ArrayList<>();
        customers.add(firstCustomer);
        customers.add(secondCustomer);

        Mockito.when(customerService.findAll()).thenReturn(customers);
        String[] expected = new String[]{"sven.hakvoort@gmail.com","info@kalshoven.nl"};

        this.mockMvc.perform(get("/v1/customers"))
            .andDo(print())
            .andExpect(status().isOk())
            .andExpect(MockMvcResultMatchers.jsonPath(".email")
                        .value(Matchers.hasSize(expected.length)))
            .andExpect(MockMvcResultMatchers.jsonPath(".email").value(Matchers.containsInAnyOrder(expected)));
    }

    @Test
    public void getAllCustomersEmpty() throws Exception {
        Mockito.when(customerService.findAll()).thenReturn(null);

        this.mockMvc.perform(get("/v1/customers"))
                .andDo(print())
                .andExpect(status().isOk());
    }

    @Test
    public void updateCustomer() throws Exception {
        Mockito.when(customerService.update(Mockito.any(Customer.class))).thenReturn(firstCustomer);

        this.mockMvc.perform(patch("/v1/customers/100"))
                .andDo(print())
                .andExpect(status().isCreated())
                .andExpect(MockMvcResultMatchers.jsonPath(".email")
                        .value(firstCustomer.getEmail()));
    }

    @Test
    public void deleteCustomer() throws Exception {

        Mockito.doNothing().when(Mockito.spy(customerService)).delete(Mockito.any(Customer.class));

        this.mockMvc.perform(delete("/v1/customers/100"))
                .andDo(print())
                .andExpect(status().isOk());
    }

}