package nl.racknet.kalshoven.klantenservice.service;

import nl.racknet.kalshoven.klantenservice.KlantenserviceApplication;
import nl.racknet.kalshoven.klantenservice.domain.Customer;
import nl.racknet.kalshoven.klantenservice.exception.CustomerNotFoundException;
import org.assertj.core.util.Lists;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.List;

import static org.junit.Assert.*;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = { KlantenserviceApplication.class })
@SpringBootTest
@ActiveProfiles("test")
public class CustomerServiceTest {

    @Autowired
    private CustomerService customerService;

    private final Customer existingCustomer = new Customer("racknet", "info@racknet.nl");
    private final Customer secondCustomer = new Customer("Kalshoven", "info@kalshoven.nl");

    @Before
    public void clean() {
        try {
            customerService.delete(customerService.findOneByName(secondCustomer.getName()));
        } catch (CustomerNotFoundException ignored) {

        }
    }

    @After
    public void restore() {
        customerService.save(existingCustomer);
    }

    @Test
    public void findOneById() {
        Customer customer = customerService.findOneByName(existingCustomer.getName());
        customer = customerService.findOneById(customer.getCustomerID());
        assertEquals(existingCustomer.getEmail(), customer.getEmail());
    }

    @Test
    public void save() {
        customerService.save(secondCustomer);

        Customer customer = customerService.findOneByName(secondCustomer.getName());

        assertEquals(secondCustomer.getEmail(), customer.getEmail());
    }

    @Test
    public void update() {
        existingCustomer.setEmail("test@test.nl");

        customerService.update(existingCustomer);

        Customer customer =  customerService.findOneByName(existingCustomer.getName());
        assertEquals(existingCustomer.getEmail(), customer.getEmail());
        existingCustomer.setEmail("info@racknet.nl");
    }

    @Test
    public void delete() {
    }

    @Test(expected = CustomerNotFoundException.class)
    public void deleteById() {
        Customer customer = customerService.findOneByName(existingCustomer.getName());
        customerService.deleteById(customer.getCustomerID());
        customerService.findOneByName(existingCustomer.getName());
    }

    @Test
    public void findAll() {
        Iterable<Customer> customers = customerService.findAll();
        List<Customer> customerList = Lists.newArrayList(customers);
        assertEquals(3, customerList.size());
    }
}