### Multi-tenant API Demo Project

Sven Hakvoort   
[sven@hakvoortdevelopment.nl]

### Dependencies ###

Mysql database, with the following databases available:  
- customerservice
- kalshoven
- testdb

Also change the username and password in application.properties and application-test.properties
as well as in kalshoven.properties

### Instructions ###

The encrypted bcrypt password in security.properties is test. you can substitute this with your own bcrypt encrypted password

### Usage examples ###

In order to do a request, request a 0auth token to the url:

localhost:8080/oauth/token


pass the client-id from security.properties with the secret as a basic auth type (i recommend using Postman for this)

![Scheme](https://bitbucket.org/svenhakvoort/api-basic-setup-multi-tenant-setup-oauth-secured/raw/4c38191ad72d6eeae0648f87c5424615d9f57859/demo-images/authorization-demo.png)

pass a username and password as x-www-form-urlencoded data, i.e. username=shakvoort&password=test&grant_type=password

![Scheme](https://bitbucket.org/svenhakvoort/api-basic-setup-multi-tenant-setup-oauth-secured/raw/df90f51ade610cea61f5dda02f07b8bcdf33a06f/demo-images/form-demo.png)

To do a new request pass the access_token in the authentication header as shown below:

![Scheme](https://bitbucket.org/svenhakvoort/api-basic-setup-multi-tenant-setup-oauth-secured/raw/4c38191ad72d6eeae0648f87c5424615d9f57859/demo-images/auth-demo.png)


